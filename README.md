# openinstallmedia
openinstallmedia is an open source install media creation tool for macOS 

## openinstallmacos
openinstallmedia is an open source installer tool for macOS. It is highly recommend you don't use this tool on your own Mac.

## openupdatemacos
openinstallmedia is an open source updater tool for macOS. It is highly recommend you don't use this tool on your own Mac.

## openprebootmedia
openinstallmedia is an open source Preboot media creation tool for macOS. It is highly recommend you don't use this tool on your own Mac.

## openrecoverymedia
openinstallmedia is an open source Recovery media creation tool for macOS. It is highly recommend you don't use this tool on your own Mac.

## Usage

### Step 1

Download the latest version of openinstallmedia from the GitHub releases page.

You can also download it from the Homebrew repository that is now available [here](https://gitlab.com/julianfairfax/package-repo).

### Step 2

Unzip the download and open Terminal. Type chmod +x and drag the script file to Terminal, then hit enter. Then drag the script file to Terminal, and hit enter.

## License
The following files and folders were created by me and are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- openinstallmacos.sh
- openinstallmedia.sh
- openprebootmedia.sh
- openrecoverymedia.sh
- openupdatemacos.sh

The following files and folders were created by Apple and are licensed under their licenses:
- resources/dm, from [10.13.6 security update 2020-004 recovery update](http://swcdn.apple.com/content/downloads/14/54/001-08570-A_XCP1PVIXQK/xg4hvg6zgpoxonqkgiw2e2cqo1c0isl97q/SecUpd2020-004HighSierra.RecoveryHDUpdate.pkg)